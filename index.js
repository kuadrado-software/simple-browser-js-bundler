const browserify = require("browserify");
const Uglify = require("uglify-js");
const fs = require("fs");

function bundle(input_path, output_path, options = {}) {
    if (options.minify) {
        const bundle = browserify()
            .add(input_path)
            .bundle();
        let stream = "";
        bundle.on("data", chunk => stream += chunk);
        bundle.on("end", () => {
            const minified = Uglify.minify(stream);
            const out = fs.createWriteStream(output_path);
            out.write(minified.code);
        });
    } else {
        browserify()
            .add(input_path)
            .bundle()
            .pipe(fs.createWriteStream(output_path));
    }
}

module.exports = {
    bundle
};