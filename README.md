# Simple Browser JS Bundler

**An excessively small and simple API to build a javascript bundle**

---

## Usage

This package only exposes one function `bundle`, wich is actually just a helper to pipe Browserify and Uglify together without any additional dependencies.

SYNTAX : `bundle(input_path<String>, output_path<String> [,options<Object>])`;

-   The input_path argument must be the absolute path of the entry point file of the code you want to bundle. All required files from there will be resolved by Browserify.

-   The ouput_path must be the absolute path string of the bundle js file.

-   The option argument can be passed a `minify` flag, wich will make the output file minified.

----------------------------

### Example

```js
const { bundle } = require("simple-browser-js-bundler");
const dir = process.cwd();

bundle(
    `${dir}/src/main.js`,
    `${dir}/public/bundle.js`, 
    { minify: true }
);
```

And voilà, that's pretty much it.